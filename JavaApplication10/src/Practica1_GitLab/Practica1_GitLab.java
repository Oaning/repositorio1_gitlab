package Practica1_GitLab;

/**
 * Entornos de Desarrollo
 * Practica1_GitLab
 * 09/11/2021
 * @author Oana
 */
public class Practica1_GitLab {
    public static void main(String[] args) {
        System.out.println("Mi primer commit desde NetBeans.");
        System.out.println("Commit desde GitLab");
        for(int altura = 1; altura<=5; altura++)
        {

            for(int asteriscos=1; asteriscos<=altura; asteriscos++)
            {
                System.out.print("*");
            }
            System.out.println();
        }
        
        System.out.println("Tags son #etiquetas");
        System.out.println("Fork es tenedor: ");
        System.out.println("Branch es rama: copias");
        System.out.println("Merge [sic] es la mujer de Homero: compara lo que tengas en tu rama principal del proyecto con la rama y pregunta qué modificaciones quieres conservar.");
        System.out.println("Fetch es un pókimon: para igualar en un punto lo que tenemos en remoto con el local.");
        System.out.println("Revert es un escritor español y recover para recuperar: revert es para volver a una versión anterior.");
        System.out.println("Commit versión 4, qué hostias es un commit?");
    }
}
